# Canvas

*  版本：1.2.2
*  作者：InFaNg

## 使用
1. 在`<head>`中引入Canvas文件
    `<link rel="stylesheet" href="https://infang.github.io/Canvas/dist/1.2.2/canvas.min.css" />`
2. 为想要添加动画效果的元素赋予以下一个类（具体效果可参考[Canvas 演示](http://infang.github.io/Canvas/)）

####过渡动画
  * `appear`
  * `appear-expand`
  * `appear-reduce`
  * `appear-top`
  * `disappear-top`
  * `appear-right`
  * `disappear-right`
  * `appear-bottom`
  * `disappear-bottom`
  * `appear-left`
  * `disappear-left`
  * `appear-rotate`
  * `disappear-rotate`
  * `appear-rotate-x`
  * `appear-rotate-y`
  * `disappear-rotate-x`
  * `disappear-rotate-y`
  * `flash`
  * `appear-scale-x`
  * `appear-scale-y`
  * `disappear-scale-x`
  * `disappear-scale-y`
  
####循环动画
  * `float`
  
  ## License
Canvas is licensed under the MIT license. (http://opensource.org/licenses/MIT)
